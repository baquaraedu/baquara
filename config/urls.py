from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.views import defaults as default_views
from django.views.generic import TemplateView
from rest_framework.documentation import include_docs_urls
from allauth.account.views import confirm_email

from baquara.users.views import FacebookLogin, TwitterLogin, \
    FacebookConnect, TwitterRequestToken, TwitterAccessToken, TwitterConnect, OAuth2UserInfoView

urlpatterns = [

    # Django Admin, use {% url 'admin:index' %}
    url(settings.ADMIN_URL, admin.site.urls),

    # Docs
    url(r'^api/docs/', include_docs_urls(title='baquara API Docs', public=False)),

    # Address utilities
    url(r'^api/address/', include('baquara.cities_api.urls', namespace='cities')),

    # Pages management
    url(r'^api/pages/', include('baquara.pages.urls', namespace='pages')),

    # User management
    url(r'^api/', include('baquara.users.urls', namespace='users')),
    # url(r'^api/profile/', include('baquara.users.urls', namespace='users')),
    url(r'^rest-auth/facebook/$', FacebookLogin.as_view(), name='fb_login'),
    url(r'^rest-auth/twitter/$', TwitterLogin.as_view(), name='tw_login'),
    url(r'^rest-auth/facebook/connect/$', FacebookConnect.as_view(), name='fb_connect'),
    url(r'^rest-auth/twitter/request_token/$', TwitterRequestToken.as_view(), name='twitter_request_token'),
    url(r'^rest-auth/twitter/access_token/$', TwitterAccessToken.as_view(), name='twitter_access_token'),
    url(r'^rest-auth/twitter/connect/$', TwitterConnect.as_view(), name='twitter_connect'),
    url(r'^rest-auth/', include('rest_auth.urls')),


    url(r'^rest-auth/registration/', include('rest_auth.registration.urls')),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^', include('django.contrib.auth.urls')),

    ## Dummy urls only to create reverse
    url(r'^recuperar-senha/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', TemplateView.as_view(), name='password_reset_confirm'),
    url(r'^perfil/editar/twitter/connect/callback$', TemplateView.as_view(), name='twitter_connect_callback'),

    # courses-courses urls
    url(r'^api/', include('courses.urls', namespace='course')),
    url(r'^api/', include('courses.certification.urls', namespace='courses_certification')),
    url(r'^legacy/', include('courses_legacy.urls', namespace='courses_legacy')),

    # ckeditor upload images in flatpages (and possible others)
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),

    # Provider to let other apps auth herel
    url(r'^oauth/', include('oauth2_provider.urls', namespace='oauth2_provider')),
    url(r'^oauth/me/?', OAuth2UserInfoView.as_view(), name='oauth2_provider_userinfo'),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if 'discussion' in settings.INSTALLED_APPS:
    urlpatterns += [url(r'^discussion/', include('discussion.urls', namespace='discussion')),]

if 'cards' in settings.INSTALLED_APPS:
    urlpatterns += [url(r'^cards/', include('cards.urls', namespace='cards')),]

# if 'courses_notifications.apps.CoursesNotificationsConfig' in settings.INSTALLED_APPS:
#     urlpatterns += [url(r'^courses_notifications/', include('courses_notifications.urls', namespace='courses_notifications')),]

if settings.TIMTEC_THEMES_COMPAT:
    urlpatterns += [
        url(r'', include('courses_legacy.urls', namespace='courses_legacy_compat')),
    ]

if settings.CHAT:
    urlpatterns += [
        url(r'^chat/', include('baquara.chat.urls', namespace='chat')),
    ]

if 'rosetta' in settings.INSTALLED_APPS:
    urlpatterns += [
        url(r'^rosetta/', include('rosetta.urls'))
    ]

if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        url(r'^400/$', default_views.bad_request, kwargs={'exception': Exception('Bad Request!')}),
        url(r'^403/$', default_views.permission_denied, kwargs={'exception': Exception('Permission Denied')}),
        url(r'^404/$', default_views.page_not_found, kwargs={'exception': Exception('Page not Found')}),
        url(r'^500/$', default_views.server_error),
    ]
    if 'debug_toolbar' in settings.INSTALLED_APPS:
        import debug_toolbar
        urlpatterns = [
            url(r'^__debug__/', include(debug_toolbar.urls)),
        ] + urlpatterns
