"""
Django settings for baquara project.

For more information on this file, see
https://docs.djangoproject.com/en/dev/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/dev/ref/settings/
"""
import environ

ROOT_DIR = environ.Path(__file__) - 3  # (baquara/config/settings/base.py - 3 = baquara/)
APPS_DIR = ROOT_DIR.path('baquara')

# Load operating system environment variables and then prepare to use them
env = environ.Env()

# APP CONFIGURATION
# ------------------------------------------------------------------------------
DJANGO_APPS = [
    # Default Django apps:
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.flatpages',

    'dal',
    'dal_select2',
]
THIRD_PARTY_APPS = [
    # needed for allauth templates
    'crispy_forms',
    'allauth',  # registration
    'allauth.account',  # registration
    'allauth.socialaccount',  # registration
    'allauth.socialaccount.providers.facebook',
    'allauth.socialaccount.providers.twitter',
    'django_filters',
    'rest_framework',
    'rest_framework.authtoken',
    'rest_auth',
    'rest_auth.registration',
    'corsheaders',
    'cities_light',

    # legacy apps deps, refactor one day...
    'compressor',

    # pages admin editor related
    'ckeditor',
    'ckeditor_uploader',

    'easy_thumbnails',
    'oauth2_provider',
    'django_celery_beat',
    'djcelery_email',
    'actstream',
    'modeltranslation',
    # Admin
    'django.contrib.admin',

    'discussion',
    'taggit',
    'cards',
]

# Apps specific for this project go here.
LOCAL_APPS = [
    # custom users app
    'baquara.users.apps.UsersConfig',
    'baquara.pages.apps.PagesConfig',
    'courses.apps.CoursesConfig',
    'courses.certification.apps.CoursesCertificationConfig',
    'courses.scheduling.apps.CoursesSchedulingConfig',
    'courses.reports.apps.CoursesReportsConfig',
    'courses.stats.apps.CoursesStatsConfig',
    'courses_learning_objects.apps.CoursesLearningObjectsConfig',
    'courses_legacy.apps.CoursesLegacyConfig',
    'courses_notifications.apps.CoursesNotificationsConfig',
    'courses_legacy.administration',
    'courses_frontend.apps.CoursesFrontendConfig'
]

LOCAL_ENV_APPS = env.list('DJANGO_LOCAL_ENV_APPS', default=[])

# See: https://docs.djangoproject.com/en/dev/ref/settings/#installed-apps
INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS + LOCAL_ENV_APPS

# MIDDLEWARE CONFIGURATION
# ------------------------------------------------------------------------------

MIDDLEWARE = [
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # XFrameOptions disabled needed to login iframe
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

# MIGRATIONS CONFIGURATION
# ------------------------------------------------------------------------------
MIGRATION_MODULES = {
    'sites': 'baquara.contrib.sites.migrations'
}

# DEBUG
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#debug
DEBUG = env.bool('DJANGO_DEBUG', False)

# FIXTURE CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-FIXTURE_DIRS
FIXTURE_DIRS = (
    str(APPS_DIR.path('fixtures')),
)

# EMAIL CONFIGURATION
# ------------------------------------------------------------------------------
EMAIL_BACKEND = env('DJANGO_EMAIL_BACKEND', default='djcelery_email.backends.CeleryEmailBackend')
CELERY_EMAIL_BACKEND = env('DJANGO_EMAIL_BACKEND', default='django.core.mail.backends.smtp.EmailBackend')
CELERY_EMAIL_TASK_CONFIG = {
    'name': 'djcelery_email_send',
    'ignore_result': False,
}
# How many email messages must be processed by each Celery job
CELERY_EMAIL_CHUNK_SIZE = 1
# How many email addresses must be included in each email message for ProfessorMessage
PROFESSOR_MESSAGE_CHUNK_SIZE = env.int('DJANGO_PROFESSOR_MESSAGE_CHUNK_SIZE', default=50)

# MANAGER CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#admins
ADMINS = [
    ("""hacklab""", 'sysadmin@hacklab.com.br'),
]

# See: https://docs.djangoproject.com/en/dev/ref/settings/#managers
MANAGERS = ADMINS

# DATABASE CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#databases
# Raises ImproperlyConfigured exception if database variables aren't in os.environ
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'HOST': env('POSTGRES_HOST', default='postgres'),
        'NAME': env('POSTGRES_DB'),
        'USER': env('POSTGRES_USER'),
        'PASSWORD': env('POSTGRES_PASSWORD'),
    },
}
DATABASES['default']['ATOMIC_REQUESTS'] = True


# GENERAL CONFIGURATION
# ------------------------------------------------------------------------------
# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'America/Sao_Paulo'
CELERY_ENABLE_UTC = False

# See: https://docs.djangoproject.com/en/dev/ref/settings/#language-code
#LANGUAGE_CODE = 'pt-br'

# Django Model translation configs
# See: https://django-modeltranslation.readthedocs.io
I18N_SUPPORT = env('I18N_SUPPORT', default=True)

gettext = lambda s: s
LANGUAGES = (
    ('pt-br', gettext('Portuguese')),
    ('es', gettext('Spanish')),
    ('en', gettext('English')),
)
MODELTRANSLATION_LANGUAGES = env.tuple('MODELTRANSLATION_LANGUAGES', default=('pt-br', 'en', 'es'))
MODELTRANSLATION_DEFAULT_LANGUAGE = env.str('MODELTRANSLATION_DEFAULT_LANGUAGE', default='pt-br')
MODELTRANSLATION_FALLBACK_LANGUAGES = env.tuple(
    'MODELTRANSLATION_FALLBACK_LANGUAGES', default=(MODELTRANSLATION_DEFAULT_LANGUAGE,)
)
MODELTRANSLATION_AUTO_POPULATE = True
MODELTRANSLATION_TRANSLATION_FILES = (
    'courses.translation',
)
MODELTRANSLATION_CUSTOM_FIELDS = ('JSONField',)

# See: https://docs.djangoproject.com/en/dev/ref/settings/#site-id
SITE_ID = 1

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-i18n
USE_I18N = True

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-l10n
USE_L10N = True

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-tz
USE_TZ = True

# Request body maximum size (defaults to 50MB)
# See: https://docs.djangoproject.com/en/2.2/ref/settings/#data-upload-max-memory-size
DATA_UPLOAD_MAX_MEMORY_SIZE = env.int('DATA_UPLOAD_MAX_MEMORY_SIZE', default=52428800)

# FILE_UPLOAD_PERMISSIONS
# See: https://docs.djangoproject.com/en/2.2/ref/settings/#std:setting-FILE_UPLOAD_PERMISSIONS
FILE_UPLOAD_PERMISSIONS = 0o755;

# TEMPLATE CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#templates
TEMPLATES = [
    {
        # See: https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-TEMPLATES-BACKEND
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        # See: https://docs.djangoproject.com/en/dev/ref/settings/#template-dirs
        'DIRS': [
            str(APPS_DIR.path('templates')),
        ],
        'OPTIONS': {
            # See: https://docs.djangoproject.com/en/dev/ref/settings/#template-debug
            'debug': DEBUG,
            # See: https://docs.djangoproject.com/en/dev/ref/settings/#template-loaders
            # https://docs.djangoproject.com/en/dev/ref/templates/api/#loader-types
            'loaders': [
                'courses_legacy.core.loaders.TimtecThemeLoader',
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
            ],
            # See: https://docs.djangoproject.com/en/dev/ref/settings/#template-context-processors
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                # legacy stuff
                'courses_legacy.core.context_processors.site_settings',
                # 'courses_legacy.core.context_processors.contact_form',
                'courses_legacy.core.context_processors.get_current_path',
                # 'courses_legacy.core.context_processors.terms_acceptance_required',
            ],
        },
    },
]

# See: http://django-crispy-forms.readthedocs.io/en/latest/install.html#template-packs
CRISPY_TEMPLATE_PACK = 'bootstrap4'

# STATIC FILE CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-root
STATIC_ROOT = str(ROOT_DIR('staticfiles'))

# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-url
STATIC_URL = '/static/'

# See: https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#std:setting-STATICFILES_DIRS
STATICFILES_DIRS = [
    str(APPS_DIR.path('static')),
]

# See: https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#staticfiles-finders
STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'compressor.finders.CompressorFinder',
]

# MEDIA CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#media-root
MEDIA_ROOT = str(APPS_DIR('media'))

# See: https://docs.djangoproject.com/en/dev/ref/settings/#media-url
MEDIA_URL = '/media/'

# URL Configuration
# ------------------------------------------------------------------------------
ROOT_URLCONF = env.str('DJANGO_ROOT_URLCONF', 'config.urls')

# See: https://docs.djangoproject.com/en/dev/ref/settings/#wsgi-application
WSGI_APPLICATION = 'config.wsgi.application'

# PASSWORD STORAGE SETTINGS
# ------------------------------------------------------------------------------
# See https://docs.djangoproject.com/en/dev/topics/auth/passwords/#using-argon2-with-django
PASSWORD_HASHERS = [
    'django.contrib.auth.hashers.Argon2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher',
    'django.contrib.auth.hashers.BCryptSHA256PasswordHasher',
    'django.contrib.auth.hashers.BCryptPasswordHasher',
]

# PASSWORD VALIDATION
# https://docs.djangoproject.com/en/dev/ref/settings/#auth-password-validators
# ------------------------------------------------------------------------------

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    #{
    #    'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    #},
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# AUTHENTICATION CONFIGURATION
# ------------------------------------------------------------------------------
AUTHENTICATION_BACKENDS = [
    'django.contrib.auth.backends.ModelBackend',
    'allauth.account.auth_backends.AuthenticationBackend',
]

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.TokenAuthentication',
    ),
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticatedOrReadOnly',
    ),
    'DEFAULT_FILTER_BACKENDS': (
        'django_filters.rest_framework.DjangoFilterBackend',
    ),
}

REST_AUTH_REGISTER_SERIALIZERS = {
    'REGISTER_SERIALIZER': 'baquara.users.serializers.RegistrationSerializer'
}

# django-all-auth configs
# ------------------------------

AUTH_USER_MODEL = 'users.User'

# Some really nice defaults
ACCOUNT_AUTHENTICATION_METHOD = "username_email"
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_EMAIL_VERIFICATION = env.str('ACCOUNT_EMAIL_VERIFICATION', 'mandatory')
ACCOUNT_USERNAME_REQUIRED = env.bool('DJANGO_ACCOUNT_USERNAME_REQUIRED', False)

REGISTRATION_DEFAULT_GROUP_NAME = env.str('DJANGO_REGISTRATION_DEFAULT_GROUP_NAME', default='students')

# ACCOUNT_USER_MODEL_USERNAME_FIELD = env.str(
#     'DJANGO_ACCOUNT_USER_MODEL_USERNAME_FIELD',
#     default='username',
# )
ACCOUNT_ADAPTER = env.str('DJANGO_ACCOUNT_ADAPTER', default='allauth.account.adapter.DefaultAccountAdapter')
ACCOUNT_UNIQUE_EMAIL = True

ACCOUNT_EMAIL_CONFIRMATION_EXPIRE_DAYS = 10

# ACCOUNT_EMAIL_SUBJECT_PREFIX = "[] "

ACCOUNT_REQUIRED_FIELDS = env.list('DJANGO_ACCOUNT_REQUIRED_FIELDS', default=['name', ])
# SOCIALACCOUNT_EMAIL_VERIFICATION = False
ACCOUNT_FORMS = {
    'signup': env.str(
        'DJANGO_ACCOUNT_SIGNUP_FORM_CLASS',
        default='baquara.users.forms.CustomSignupForm'
        )
    }

LOGIN_URL = env.str('DJANGO_LOGIN_URL', 'account_login')
LOGIN_REDIRECT_URL = env.str('DJANGO_LOGIN_REDIRECT_URL', 'courses_frontend:base_spa')

ACCOUNT_ALLOW_REGISTRATION = env.bool('DJANGO_ACCOUNT_ALLOW_REGISTRATION', True)

SOCIALACCOUNT_ADAPTER = 'baquara.users.adapters.SocialAccountAdapter'
SOCIALACCOUNT_QUERY_EMAIL = True

SOCIALACCOUNT_PROVIDERS = {
    'facebook': {
        'METHOD': 'js_sdk',
        'SCOPE': ['email', 'public_profile', 'user_friends'],
        'AUTH_PARAMS': {'auth_type': 'reauthenticate'},
        'INIT_PARAMS': {'cookie': True},
        'FIELDS': [
            'id',
            'email',
            'name',
            'first_name',
            'last_name',
            'verified',
            'locale',
            'timezone',
            'link',
            'gender',
            'updated_time',
        ],
        'EXCHANGE_TOKEN': True,
        'VERIFIED_EMAIL': False,
        'VERSION': 'v2.12',
    }
}

# SLUGLIFIER
AUTOSLUG_SLUGIFY_FUNCTION = 'slugify.slugify'


# Custom Admin URL, use {% url 'admin:index' %}
ADMIN_URL = env('DJANGO_ADMIN_URL', default=r'^django/admin/')

# Celery
# ------------------------------------------------------------------------------
REDIS_URL = env('REDIS_URL', default='redis://redis:6379')
if USE_TZ:
    # http://docs.celeryproject.org/en/latest/userguide/configuration.html#std:setting-timezone
    CELERY_TIMEZONE = TIME_ZONE
    CELERY_ENABLE_UTC = False
# http://docs.celeryproject.org/en/latest/userguide/configuration.html#std:setting-broker_url
CELERY_BROKER_URL = env("CELERY_BROKER_URL", default='redis://redis:6379/0')
# http://docs.celeryproject.org/en/latest/userguide/configuration.html#std:setting-result_backend
CELERY_RESULT_BACKEND = CELERY_BROKER_URL
# http://docs.celeryproject.org/en/latest/userguide/configuration.html#std:setting-accept_content
CELERY_ACCEPT_CONTENT = ["json", "pickle",]
# http://docs.celeryproject.org/en/latest/userguide/configuration.html#std:setting-task_serializer
CELERY_TASK_SERIALIZER = "json"
# http://docs.celeryproject.org/en/latest/userguide/configuration.html#std:setting-result_serializer
CELERY_RESULT_SERIALIZER = "json"
# http://docs.celeryproject.org/en/latest/userguide/configuration.html#task-time-limit
# TODO: set to whatever value is adequate in your circumstances
CELERY_TASK_TIME_LIMIT = 6 * 60
# http://docs.celeryproject.org/en/latest/userguide/configuration.html#task-soft-time-limit
# TODO: set to whatever value is adequate in your circumstances
CELERY_TASK_SOFT_TIME_LIMIT = 5 * 60
# http://docs.celeryproject.org/en/latest/userguide/configuration.html#beat-scheduler
CELERY_BEAT_SCHEDULER = "django_celery_beat.schedulers:DatabaseScheduler"

# Django-cards
# ------------------------------------------------------------------------------
DJANGO_CARDS_ADMIN_GROUPS = env.list('DJANGO_LOCAL_ENV_APPS', default=[''])

# Django cities light
# ------------------------------------------------------------------------------

# http://download.geonames.org/export/dump/iso-languagecodes.txt
CITIES_LIGHT_TRANSLATION_LANGUAGES = ['pt', 'abbr', 'en', 'es']

# http://www.geonames.org/export/codes.html
CITIES_LIGHT_INCLUDE_CITY_TYPES = [
    'PPL', 'PPLA', 'PPLA2', 'PPLA3', 'PPLA4', 'PPLC', 'PPLF', 'PPLG', 'PPLL', 'PPLR', 'PPLS', 'STLMT',
]

# API CEP Aberto
# ------------------------------------------------------------------------------
CEP_ABERTO_API_KEY = env.str('CEP_ABERTO_API_KEY', '')
CEP_ABERTO_URL = env.str('CEP_ABERTO_URL', '')

RECAPTCHA_SECRET_KEY = env.str('RECAPTCHA_SECRET_KEY', '')

# django-ckeditor
# ------------------------------------------------------------------------------
CKEDITOR_UPLOAD_PATH = "uploads/"
CKEDITOR_IMAGE_BACKEND = "pillow"
CKEDITOR_BROWSE_SHOW_DIRS = True
CKEDITOR_ALLOW_NONIMAGE_FILES = True

CKEDITOR_CONFIGS = {
    'default': {
        'skin': 'moono-lisa',
        'toolbar_Basic': [
            ['Source', '-', 'Bold', 'Italic']
        ],
        'toolbar_Full': [
            ['Styles', 'Format', 'Bold', 'Italic', 'Underline', 'Strike', 'SpellChecker', 'Undo', 'Redo'],
            ['Link', 'Unlink', 'Anchor'],
            ['Image', 'Flash', 'Table', 'HorizontalRule'],
            ['TextColor', 'BGColor'],
            ['Source'],
        ],
        'toolbar': 'Full',
        'height': 291,
        'width': 835,
        'filebrowserWindowWidth': 940,
        'filebrowserWindowHeight': 725,
        'extraPlugins': ','.join([
            'uploadimage', # the upload image feature
            # your extra plugins here
            # 'div',
            'autolink',
            'autoembed',
            'embedsemantic',
            'autogrow',
            # # 'devtools',
            # 'widget',
            # 'lineutils',
            'clipboard',
            # 'dialog',
            # 'dialogui',
            # 'elementspath'
        ]),
        'filebrowserBrowseUrl': '/ckeditor/browse/',
        'filebrowserUploadUrl': '/ckeditor/upload/'
    }
}

# RocketChat configuration
# ------------------------------------------------------------------------------
CHAT = env.bool('DJANGO_CHAT', False)
if CHAT:
    INSTALLED_APPS += ['baquara.chat.apps.ChatConfig', ]

ROCKET_CHAT = {
    'address': env.str(
        'DJANGO_ROCKET_CHAT_ADDRESS',
        default='http://localhost:3000/',
    ),
    'service':  env.str(
        'DJANGO_ROCKET_CHAT_SERVICE',
        default='RocketChat',
    ),
    'auth_token': env.str(
        'DJANGO_ROCKET_CHAT_AUTH_TOKEN',
        default='',
    ),
    'user_id': env.str(
        'DJANGO_ROCKET_CHAT_USER_ID',
        default='',
    ),
}

OAUTH2_PROVIDER = {
    'SCOPES': {
        'read': 'Read scope',
        'write': 'Write scope',
        'openid': 'Openid scope',
    },
}

# Django cors (needed only by RocketChat)
# ------------------------------------------------------------------------------
CORS_ORIGIN_ALLOW_ALL = env.bool('CORS_ORIGIN_ALLOW_ALL', default=False)
CORS_ORIGIN_WHITELIST = [
    ROCKET_CHAT['address'],
]
CORS_ORIGIN_WHITELIST += env.list('DJANGO_CORS_ORIGIN_WHITELIST', default=[])

CSRF_TRUSTED_ORIGINS = [
    ROCKET_CHAT['address'].split("//")[-1].split("/")[0].split('?')[0],
]
CSRF_TRUSTED_ORIGINS += env.list('DJANGO_CSRF_TRUSTED_ORIGINS', default=[])

SESSION_COOKIE_SAMESITE = env.bool('DJANGO_SESSION_COOKIE_SAMESITE', default=None)
CORS_ALLOW_CREDENTIALS = env.bool('DJANGO_SCORS_ALLOW_CREDENTIALS', default=True)

# Legacy Defaults, review everything. Everyting beyond here is legacy
# ------------------------------------------------------------------------------

TERMS_ACCEPTANCE_REQUIRED = env.bool('DJANGO_TERMS_ACCEPTANCE_REQUIRED', default=True)

# CERTIFICATE_SIZE = (862, 596)
# PHANTOMJS_PATH = ROOT_DIR.path('node_modules/phantomjs-prebuilt/bin/phantomjs')

YOUTUBE_API_KEY = env.str('YOUTUBE_API_KEY', '')

GOOGLE_ANALYTICS_JS_PROPERTY_ID = env.str('GOOGLE_ANALYTICS_JS_PROPERTY_ID', '')

# [legacy] Django compressor stuff
COMPRESS_OFFLINE = True

COMPRESS_PRECOMPILERS = (
    ('text/x-scss', 'django_libsass.SassCompiler'),
    ('module', 'cat {infile} > {outfile}'),
)

SITE_DOMAIN = env.str('DJANGO_SITE_DOMAIN', default='')
SITE_HOME = env.str('DJANGO_SITE_HOME', default='')
SITE_NAME = env.str('DJANGO_SITE_NAME', default='')
SITE_NAME_EN = env.str('DJANGO_SITE_NAME_EN', SITE_NAME)
SITE_NAME_ES = env.str('DJANGO_SITE_NAME_ES', SITE_NAME)
SITE_NAME_PT_BR = env.str('DJANGO_SITE_NAME_PT_BR', SITE_NAME)

#
# Theme related options
#
THEME = env.str('BAQUARA_THEME', default='courses_frontend')

THEMES_DIR = ''
TIMTEC_THEME = env.str('DJANGO_TIMTEC_THEME', default=THEME)
TIMTEC_THEMES_COMPAT = env.bool('DJANGO_TIMTEC_THEMES_COMPAT', default=False)

COMPRESS_OFFLINE_CONTEXT = {
    'main_scss': 'scss-' + THEME + '/main.scss'
}

if TIMTEC_THEMES_COMPAT:
    # FIXME remove this ACCOUNT_LOGOUT_ON_GET, see more here:
    # https://django-allauth.readthedocs.io/en/latest/views.html?highlight=login%20redirect#logout-account-logout
    ACCOUNT_LOGOUT_ON_GET = True
