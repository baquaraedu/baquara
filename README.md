# Projeto Django Base para MOOCs

Projeto Django baseado no padrão [cookiecutter-django](http://cookiecutter-django.readthedocs.io/en/latest) e com diversas adaptações para uso na infraestrutura Docker-Rancher do hacklab/.

## Ambiente de desenvolvimento

Para clonar este repositório e seus [submódulos](https://git-scm.com/book/en/v2/Git-Tools-Submodules), execute:

`git clone --recursive git@gitlab.com:open-source/base-django-mooc.git`

Levante o ambiente de desenvolvimento com `docker-compose -f local.yml up` e acesse [localhost:8000](http://localhost:8000).

Se você já tiver a pasta de um submódulo no seu ambiente local, mas ela estiver vazia, execute o seguinte comando dentro dela:

```
git submodule sync
git submodule update --init
```
## Importanto Bando de dados de testes (ou da produção)

Neste exemplo, apagamos e recriamos o banco para garantir que estamos importando sobre uma base limpa.
Este exemplo cobre a importação feita a partir de um binário (psqlc), adapte o os comandos para usar com outros formatos.

```
# Sobe somente container do postgres, importantes senão o django vai bloquer alterações no banco
docker-compose up postgres
# copia backup para dentro do container (isso pode ser melhorado)
docker cp backup.psqlc postgres_1:/backup.psqlc
docker-compose exec postgres sh -c "dropdb -U\$POSTGRES_USER django"
docker-compose exec postgres sh -c "createdb -U\$POSTGRES_USER django"
docker-compose exec postgres sh -c "pg_restore -U\$POSTGRES_USER -O -x -n public -d django backup.psqlc"
```

## Importando dados para o django-geocities-light (Cidades e Estados)
```
docker exec -it base-django-mooc_django_1 ./manage.py cities_light --progress -v 3
```

## Testes

Existem duas maneiras de se executar os testes automatizados localmente:

- Você já executou o comando `docker-compose -f local.yml up` e o servidor está funcionando.

```
docker-compose -f local.yml exec django pytest
```

- Você deseja apenas executar os testes sem necessariamente levantar o servidor. Antes é necessário construir a imagem do backend e disponibilizar o banco de dados para então executar o pytest via `docker run`

```
docker build -f compose/test/django/Dockerfile -t django_test .
docker run -d --env-file=./compose/test/test_env --name=postgres_test postgres:9.6
docker run --env-file=./compose/test/test_env --link=postgres_test:postgres \
  django_test /test.sh
```

## Variáveis de ambiente
### Banco de dados
- POSTGRES_HOST - opcional; padrão 'postgres'
- POSTGRES_DB - obrigatório
- POSTGRES_USER - obrigatório
- POSTGRES_PASSWORD - obrigatório

### Email
- MAILGUN_SENDER_DOMAIN - obrigatório em produção
- DJANGO_DEFAULT_FROM_EMAIL - obrigatório em produção
- DJANGO_MAILGUN_API_KEY - obrigatório em produção

### Django
- DJANGO_ALLOWED_HOSTS - obrigatório em produção
- DJANGO_ADMIN_URL - opcional
- DJANGO_SETTINGS_MODULE - opcional; use `config.settings.production` em produção
- DJANGO_ACCOUNT_ALLOW_REGISTRATION - opcional; padrão True
- DJANGO_SECRET_KEY - obrigatório em produção
- USE_CACHE - opcional; padrão True

### Redis
- REDIS_URL - obrigatório em produção; exemplo: `redis://127.0.0.1:6379`

### Sentry
- DJANGO_SENTRY_DSN - opcional; só válido em produção

## Depoly em produção

Um exemplo de deploy em produção pode ser encontrado no arquivo `production.yml`.

Para rodá-lo localmente, e assim ter o máximo de aderência com o ambiente final, cire um arquivo `.env` baseado em `env.example` com as configurações necessárias e execute:

```
docker-compose -f production.yml up
```

## Integrações de deploy
**Commits no branch `master`** fazem releases da versão em **desenvolvimento**.

**Tags** fazem releases em [**produção**](http://example.com/).
