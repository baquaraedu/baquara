============
Installation
============

At the command line::

    $ easy_install django-courses-notifications

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv django-courses-notifications
    $ pip install django-courses-notifications
