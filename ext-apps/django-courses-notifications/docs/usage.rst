=====
Usage
=====

To use Django Courses Notifications in a project, add it to your `INSTALLED_APPS`:

.. code-block:: python

    INSTALLED_APPS = (
        ...
        'courses_notifications.apps.DjangoCoursesNotificationsConfig',
        ...
    )

Add Django Courses Notifications's URL patterns:

.. code-block:: python

    from courses_notifications import urls as courses_notifications_urls


    urlpatterns = [
        ...
        url(r'^', include(courses_notifications_urls)),
        ...
    ]
