# Generated by Django 2.2.5 on 2019-09-21 14:57

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('discussion', '0022_delete_unreadnotification'),
        ('courses_learning_objects', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='UnreadNotification',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('counter', models.IntegerField(blank=True, null=True)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='user')),
            ],
        ),
        migrations.CreateModel(
            name='AnswerNotification',
            fields=[
                ('topicnotification_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='discussion.TopicNotification')),
                ('activity', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='courses_learning_objects.LearningObject')),
            ],
            bases=('discussion.topicnotification',),
        ),
    ]
