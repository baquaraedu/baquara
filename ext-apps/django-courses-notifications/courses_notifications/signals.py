from django.db.models.signals import post_save
from django.dispatch import receiver

from courses_legacy.activities.models import Answer
from .models import UnreadNotification, AnswerNotification


@receiver(post_save, sender=Answer)
def answer_created_or_updated(instance, **kwargz):

    # All instructors of the corresponding course must be notified
    notifiable_users = instance.activity.unit.lesson.course.professors
    try:
        topic_id = instance.given.get('topic', None)
    except AttributeError as e:
        # If "given" isn't a dictionary, there's no topic data to get
        topic_id = None

    # If this answer is not relative to a Discussion Activity, this signal must not notify professors
    if topic_id is None:
        return

    # Create the New Topic notification for appropriate users
    for user in notifiable_users.all():
        try:
            notification = AnswerNotification.objects.get(
                user=user,
                topic_id=topic_id,
                activity=instance.activity
            )
        except AnswerNotification.DoesNotExist:
            notification = AnswerNotification.objects.create(
                user=user,
                topic_id=topic_id,
                activity=instance.activity
            )

        notification.action = 'new_activity'
        notification.is_read = False
        notification.save()

        # Increase the unread count for this user in 1
        # But only if the user already has a UnreadNotification instance
        try:
            unread = UnreadNotification.objects.get(user=user)
            unread.counter += 1
            unread.save()
        except UnreadNotification.DoesNotExist:
            pass
