# -*- coding: utf-8 -*-
from django.conf.urls import include, url
from django.views.generic import TemplateView
from rest_framework import routers

from . import views


app_name = 'courses_notifications'

router = routers.SimpleRouter(trailing_slash=False)
router.register(r'answer-notification', views.AnswerNotificationViewSet)
router.register(r'unread-notification', views.UnreadNotificationViewSet)

urlpatterns = [
    url(r'', include(router.urls)),
]
