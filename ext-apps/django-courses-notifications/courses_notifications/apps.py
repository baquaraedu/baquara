# -*- coding: utf-8
from django.apps import AppConfig


class CoursesNotificationsConfig(AppConfig):
    name = 'courses_notifications'

    def ready(self):
        import courses_notifications.signals
