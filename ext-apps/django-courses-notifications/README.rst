=============================
Django Courses Notifications
=============================

.. image:: https://badge.fury.io/py/django-courses-notifications.svg
    :target: https://badge.fury.io/py/django-courses-notifications

.. image:: https://travis-ci.org/hacklabr/django-courses-notifications.svg?branch=master
    :target: https://travis-ci.org/hacklabr/django-courses-notifications

.. image:: https://codecov.io/gh/hacklabr/django-courses-notifications/branch/master/graph/badge.svg
    :target: https://codecov.io/gh/hacklabr/django-courses-notifications

Notifications support for the Django Courses app

Documentation
-------------

The full documentation is at https://django-courses-notifications.readthedocs.io.

Quickstart
----------

Install Django Courses Notifications::

    pip install django-courses-notifications

Add it to your `INSTALLED_APPS`:

.. code-block:: python

    INSTALLED_APPS = (
        ...
        'courses_notifications.apps.DjangoCoursesNotificationsConfig',
        ...
    )

Add Django Courses Notifications's URL patterns:

.. code-block:: python

    from courses_notifications import urls as courses_notifications_urls


    urlpatterns = [
        ...
        url(r'^', include(courses_notifications_urls)),
        ...
    ]

Features
--------

* TODO

Running Tests
-------------

Does the code actually work?

::

    source <YOURVIRTUALENV>/bin/activate
    (myenv) $ pip install tox
    (myenv) $ tox

Credits
-------

Tools used in rendering this package:

*  Cookiecutter_
*  `cookiecutter-djangopackage`_

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`cookiecutter-djangopackage`: https://github.com/pydanny/cookiecutter-djangopackage
