from . import views
from django.conf.urls import url


app_name = 'chat'

urlpatterns = [
    url(r'^$', views.ChatScreenView.as_view(template_name="chat.html"), name='chat'),
    url(r'^auth/$', views.RocketchatIframeAuthView.as_view(), name='rocketchat_iframe_auth'),
]
