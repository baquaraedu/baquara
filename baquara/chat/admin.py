from django.contrib import admin
from .models import ChatSyncRule, ChatRoom, ChatUser


@admin.register(ChatSyncRule)
class ChatSyncRuleAdmin(admin.ModelAdmin):
    list_display = ['name', 'created_at', 'updated_at']
    filter_horizontal = [
        'groups',
        'workspaces',
    ]
    ordering = ['created_at', 'updated_at']
    search_fields = ['name', 'description']

    autocomplete_fields = ('groups',)


@admin.register(ChatUser)
class ChatUserAdmin(admin.ModelAdmin):
    list_display = ['local_user', 'sync_rule', 'created_at', 'updated_at']
    ordering = ['local_user__username', 'created_at', 'updated_at']
    search_fields = ['local_user__username', 'local_user__name', 'local_user__email', 'remote_user']

    autocomplete_fields = ('local_user', 'sync_rule',)


@admin.register(ChatRoom)
class ChatRoomAdmin(admin.ModelAdmin):
    list_display = ['group', 'sync_rule', 'created_at', 'updated_at']
    ordering = ['group', 'created_at', 'updated_at']
    search_fields = ['group__name', 'room']

    autocomplete_fields = ('group',)
