from django.contrib.auth.models import Group
from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.conf import settings

from courses.workspaces.models import Workspace


class ChatSyncRule(models.Model):
    SYNC_TYPE_CHOICES = (
        ('undefined', _('Undefined')),
        ('groups', _('Groups')),
        ('workspaces', _('Workspaces')),
    )

    name = models.CharField(
        _('Name'),
        max_length=255
    )
    description = models.TextField(
        _('SyncRule Description'),
        blank=True,
        null=True
    )
    groups = models.ManyToManyField(
        Group,
        verbose_name=_('groups'),
        blank=True
    )
    workspaces = models.ManyToManyField(
        Workspace,
        verbose_name=_('workspaces'),
        blank=True
    )
    sync_type = models.CharField(
        choices=SYNC_TYPE_CHOICES,
        default='undefined',
        max_length=64
    )
    created_at = models.DateTimeField(
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        auto_now=True
    )

    def __str__(self):
        return self.name


class ChatRoom(models.Model):
    room = models.CharField(
        _('Room ID'),
        max_length=17
    )
    name = models.CharField(
        _('Chat Name'),
        max_length=512,
        blank=True,
        null=True,
    )
    group = models.OneToOneField(
        Group,
        verbose_name=_('Group'),
        on_delete=models.SET_NULL,
        related_name='chatroom',
        null=True
    )
    sync_rule = models.ForeignKey(
        ChatSyncRule,
        verbose_name=_('ChatSyncRule'),
        on_delete=models.SET_NULL,
        null=True
    )
    created_at = models.DateTimeField(
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        auto_now=True
    )

    def __str__(self):
        return self.group.name


class ChatUser(models.Model):
    local_user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_('Local User'),
        on_delete=models.CASCADE
    )
    remote_user = models.CharField(
        _('Remote User'),
        max_length=17
    )
    sync_rule = models.ForeignKey(
        ChatSyncRule,
        verbose_name=_('ChatSyncRule'),
        on_delete=models.SET_NULL,
        null=True
    )
    created_at = models.DateTimeField(
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        auto_now=True
    )

    def __str__(self):
        return self.local_user.username
