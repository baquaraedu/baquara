# Generated by Django 2.2.14 on 2020-07-29 19:12

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('chat', '0003_auto_20200727_2221'),
    ]

    operations = [
        migrations.AlterField(
            model_name='chatroom',
            name='group',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='chatroom', to='auth.Group', verbose_name='Group'),
        ),
    ]
