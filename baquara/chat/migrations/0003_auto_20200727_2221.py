# Generated by Django 2.2.14 on 2020-07-28 01:21

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('chat', '0002_auto_20200519_0224'),
    ]

    operations = [
        migrations.AddField(
            model_name='chatroom',
            name='name',
            field=models.CharField(blank=True, max_length=512, null=True, verbose_name='Chat Name'),
        ),
        migrations.AlterField(
            model_name='chatroom',
            name='group',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.SET_NULL, to='auth.Group', verbose_name='Group'),
        ),
    ]
