from django.apps import AppConfig


class ChatConfig(AppConfig):
    name = 'baquara.chat'
    verbose_name = "Chat"
