# Generated by Django 2.2.27 on 2022-04-26 03:53

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0002_auto_20210712_1633'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='baquaraflatpage',
            name='enable_comments_en',
        ),
        migrations.RemoveField(
            model_name='baquaraflatpage',
            name='enable_comments_es',
        ),
        migrations.RemoveField(
            model_name='baquaraflatpage',
            name='enable_comments_pt_br',
        ),
        migrations.RemoveField(
            model_name='baquaraflatpage',
            name='registration_required_en',
        ),
        migrations.RemoveField(
            model_name='baquaraflatpage',
            name='registration_required_es',
        ),
        migrations.RemoveField(
            model_name='baquaraflatpage',
            name='registration_required_pt_br',
        ),
        migrations.RemoveField(
            model_name='baquaraflatpage',
            name='template_name_en',
        ),
        migrations.RemoveField(
            model_name='baquaraflatpage',
            name='template_name_es',
        ),
        migrations.RemoveField(
            model_name='baquaraflatpage',
            name='template_name_pt_br',
        ),
        migrations.RemoveField(
            model_name='baquaraflatpage',
            name='url_en',
        ),
        migrations.RemoveField(
            model_name='baquaraflatpage',
            name='url_es',
        ),
        migrations.RemoveField(
            model_name='baquaraflatpage',
            name='url_pt_br',
        ),
    ]
