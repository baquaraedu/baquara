from .models import BaquaraFlatPage
from modeltranslation.translator import register, TranslationOptions, translator
from django.contrib.flatpages.models import FlatPage


class FlatPageTranslationOptions(TranslationOptions):
    fields = ()

class BaquaraFlatPageTranslationOptions(TranslationOptions):
    fields = (
        'title',
        'content',
    )

translator.register(FlatPage, FlatPageTranslationOptions)
translator.register(BaquaraFlatPage, BaquaraFlatPageTranslationOptions)
