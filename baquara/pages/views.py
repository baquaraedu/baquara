from .models import BaquaraFlatPage

from rest_framework import viewsets

from .serializers import FlatpageSerializer


class FlatpageViewSet(viewsets.ReadOnlyModelViewSet):

    model = BaquaraFlatPage
    queryset = BaquaraFlatPage.objects.all()
    serializer_class = FlatpageSerializer
    filter_fields = ('url',)

    def get_queryset(self):
        queryset = super(FlatpageViewSet, self).get_queryset()
        url_prefix = self.request.query_params.get('url_prefix')
        if url_prefix:
            queryset = queryset.filter(url__startswith=url_prefix)
        return queryset
