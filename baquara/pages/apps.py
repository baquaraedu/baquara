from django.apps import AppConfig


class PagesConfig(AppConfig):
    name = 'baquara.pages'
    verbose_name = "Pages"
