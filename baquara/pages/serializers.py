from rest_framework import serializers
from .models import BaquaraFlatPage


class FlatpageSerializer(serializers.ModelSerializer):

    class Meta:
        model = BaquaraFlatPage
        fields = ['id', 'url', 'title', 'content',]
