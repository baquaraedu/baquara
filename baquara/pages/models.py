from django.db import models
from django.contrib.flatpages.models import FlatPage


class BaquaraFlatPage(FlatPage):
    created_on = models.DateTimeField(auto_now_add=True)
    modified_on = models.DateTimeField(auto_now=True)
