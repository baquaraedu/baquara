from rest_framework.serializers import ModelSerializer

from cities_light.models import City, Region, Country


class CitySerializer(ModelSerializer):

    class Meta:
        model = City
        exclude = ('slug',)


class RegionSerializer(ModelSerializer):

    class Meta:
        model = Region
        exclude = ('slug',)


class CountrySerializer(ModelSerializer):

    class Meta:
        model = Country
        fields = '__all__'



