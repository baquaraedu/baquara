import json

import requests
from dal import autocomplete
from django.conf import settings
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets, relations

from cities_light.models import City, Region, Country
from rest_framework.decorators import permission_classes, api_view
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from .serializers import CountrySerializer, CitySerializer, RegionSerializer


class CitiesLightListModelViewSet(viewsets.ReadOnlyModelViewSet):

    def get_queryset(self):
        """
        Allows a GET param, 'q', to be used against name_ascii.
        """
        queryset = super(CitiesLightListModelViewSet, self).get_queryset()

        if self.request.GET.get('q', None):
            return queryset.filter(name_ascii__icontains=self.request.GET['q'])

        return queryset


class CityAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = City.objects.all()

        state = self.forwarded.get('state', None)

        if state:
            qs = qs.filter(region=state)

        if self.q:
            qs = qs.filter(name__istartswith=self.q)

        return qs


class CountryModelViewSet(CitiesLightListModelViewSet):
    serializer_class = CountrySerializer
    queryset = Country.objects.all()


class RegionModelViewSet(CitiesLightListModelViewSet):
    serializer_class = RegionSerializer
    queryset = Region.objects.all()
    filter_backends = (DjangoFilterBackend,)

    filter_fields = ('name',)

    def get_queryset(self):
        queryset = super(RegionModelViewSet, self).get_queryset()

        if self.request.GET.get('uf', None):
            return queryset.filter(alternate_names__contains=self.request.GET['uf'])

        return queryset


class CityModelViewSet(CitiesLightListModelViewSet):
    """
    ListRetrieveView for City.
    """
    serializer_class = CitySerializer
    queryset = City.objects.all()
    filter_backends = (DjangoFilterBackend,)

    filter_fields = ('region',)


    def get_queryset(self):
        """
        Allows a GET param, 'q', to be used against search_names.
        """
        queryset = super(CitiesLightListModelViewSet, self).get_queryset()

        if self.request.GET.get('q', None):
            return queryset.filter(
                search_names__icontains=self.request.GET['q'])

        return queryset


@api_view(http_method_names=['GET'])
@permission_classes((AllowAny, ))
def address_search(request, cep, *args, **kwargs):
    address_req = requests.get(settings.CEP_ABERTO_URL + '/ceps.json?cep=%s' % cep,
                               headers={'Authorization': 'Token token=%s' % settings.CEP_ABERTO_API_KEY})

    try:
        return Response(address_req.json(), status=address_req.status_code)
    except json.decoder.JSONDecodeError:
        return Response(address_req._content, status=address_req.status_code)


