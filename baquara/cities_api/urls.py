from django.conf.urls import include, url
from rest_framework import routers
from baquara.cities_api import views

router = routers.DefaultRouter()
router.register(r'cities', views.CityModelViewSet, base_name='cities-light-api-city')
router.register(r'countries', views.CountryModelViewSet,
                base_name='cities-light-api-country')
router.register(r'regions', views.RegionModelViewSet,
                base_name='cities-light-api-region')

app_name = 'cities_api'

urlpatterns = [
    url(r'^', include(router.urls)),
    url(
        r'^search/cep/(?P<cep>[0-9]+)/$',
        view=views.address_search,
        name='address-search'),
    url(
        r'^autocomplete/city/$',
        views.CityAutocomplete.as_view(),
        name='city-autocomplete',
    ),
]
