from django.contrib.auth.signals import user_logged_in
from django.dispatch import receiver
from django.contrib.auth import get_user_model


User = get_user_model()


@receiver(user_logged_in, sender=User)
def update_user_login(sender, user, **kwargs):
    user.userlogin_set.create()
    user.save()
