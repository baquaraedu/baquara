from django.apps import AppConfig


class UsersConfig(AppConfig):
    name = 'baquara.users'
    verbose_name = "Users"

    def ready(self):
        import baquara.users.signals