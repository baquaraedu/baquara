from django.contrib.auth.models import AbstractUser, Group
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings

from easy_thumbnails.files import get_thumbnailer
from easy_thumbnails.exceptions import InvalidImageFormatError

from allauth.socialaccount.models import SocialAccount
from cities_light.models import City, Region, Country
import hashlib
import re


class GenderOption(models.Model):
    name = models.CharField(
        _('Option Name'),
        null=True, max_length=255,
        blank=True
    )

    def __str__(self):
        return self.name or ''


class RaceOption(models.Model):
    name = models.CharField(
        _('Option Name'),
        null=True, max_length=255,
        blank=True
    )

    def __str__(self):
        return self.name or ''


class SexualOrientationOption(models.Model):
    name = models.CharField(
        _('Option Name'),
        null=True, max_length=255,
        blank=True
    )

    def __str__(self):
        return self.name or ''


class User(AbstractUser):

    LANGUAGES_CHOICES = (
        ('en', _('en')),
        ('es', _('es')),
        ('pt-br', _('pt-br')),
    )

    LIVING_AREA_CHOICES = (
        ('urban', _('Urban')),
        ('rural', _('Rural')),
    )

    biography = models.TextField(_('Biography'), blank=True, null=True)
    # First Name and Last Name do not cover name patterns
    # around the globe.
    name = models.CharField(
        _('User\'s name'),
        blank=True,
        max_length=255,
        null=True
    )
    occupation = models.CharField(
        _('Occupation'),
        null=True,
        blank=True,
        max_length=255
    )
    image = models.ImageField(
        _('Image'),
        blank=True,
        null=True,
        upload_to='profile_images'
    )
    gender = models.ForeignKey(
        GenderOption,
        on_delete=models.SET_NULL,
        verbose_name=_('Gender'),
        null=True,
        blank=True,
    )
    gender_other = models.CharField(
        _('Other type of gender'),
        null=True, max_length=255,
        blank=True
    )
    sexual_orientation = models.ForeignKey(
        SexualOrientationOption,
        on_delete=models.SET_NULL,
        verbose_name=_('Sexual orientation'),
        null=True,
        blank=True,
    )
    sexual_orientation_other = models.CharField(
        _('Other sexual orientation'),
        null=True, max_length=255,
        blank=True
    )
    race = models.ForeignKey(
        RaceOption,
        on_delete=models.SET_NULL,
        verbose_name=_('Ethnicity'),
        null=True,
        blank=True,
    )
    birth_date = models.DateField(
        _('Birth Date'),
        blank=True,
        null=True
    )

    # Contact info
    phone_number = models.CharField(
        _('Phone number'),
        null=True,
        blank=True,
        max_length=255
    )
    phone_number_other = models.CharField(
        _('Other phone number'),
        null=True,
        blank=True,
        max_length=255
    )

    # Localization info
    address = models.CharField(
        _('Address'),
        null=True, max_length=255,
        blank=True
    )
    address_number = models.CharField(
        _('Address number'),
        null=True, max_length=255,
        blank=True
    )
    address_extra = models.CharField(
        _('Extra information for address'),
        null=True, max_length=255,
        blank=True
    )
    neighborhood = models.CharField(
        _('Neighborhood'),
        null=True, max_length=255,
        blank=True
    )
    zipcode = models.CharField(
        _('ZIP Code'),
        null=True, max_length=255,
        blank=True
    )
    city = models.ForeignKey(
        City,
        on_delete=models.SET_NULL,
        verbose_name=_('City'),
        related_name='users',
        null=True,
        blank=True,
    )
    state = models.ForeignKey(
        Region,
        on_delete=models.SET_NULL,
        verbose_name=_('Region'),
        related_name='users',
        null=True,
        blank=True
    )
    country = models.ForeignKey(
        Country,
        on_delete=models.SET_NULL,
        verbose_name=_('Country'),
        related_name='users',
        null=True,
        blank=True,
    )
    preferred_language = models.CharField(
        _('Preferred language'),
        choices=LANGUAGES_CHOICES,
        max_length=255,
        default='en',
    )
    living_area = models.CharField(
        _('Living Area'),
        choices=LIVING_AREA_CHOICES,
        max_length=255,
        null=True,
        blank=True
    )
    # Social info
    social_facebook = models.CharField(
        _('Facebook'),
        null=True, max_length=255,
        blank=True
    )
    social_twitter = models.CharField(
        _('Twitter'),
        null=True, max_length=255,
        blank=True
    )
    social_instagram = models.CharField(
        _('Instagram'),
        null=True, max_length=255,
        blank=True
    )
    social_skype = models.CharField(
        _('Skype'),
        null=True, max_length=255,
        blank=True
    )
    social_google = models.CharField(
        _('Google'),
        null=True, max_length=255,
        blank=True
    )
    site = models.CharField(
        _('Site'),
        null=True, max_length=255,
        blank=True
    )

    # Legacy stuff
    cpf = models.CharField(max_length=11, blank=True, null=True, unique=True)
    institution = models.CharField(max_length=255, blank=True, null=True)
    date_joined = models.DateTimeField(_('Date joined'), auto_now_add=True)
    # picture = models.ImageField(_("Picture"), upload_to=hash_name('user-pictures', 'username'), blank=True)
    site = models.URLField(_('Site'), blank=True)
    accepted_terms = models.BooleanField(_('Accepted terms and condition'), default=False)


    def __str__(self):
        return '{} - {} - {}'.format(self.username, self.name, self.email)

    @property
    def image_url(self):
        if self.image:
            return self.image.url
        social_accounts = SocialAccount.objects.filter(user_id=self.id)

        for account in social_accounts:
            picture = account.get_avatar_url()
            if picture:
                return picture
        return "https://gravatar.com/avatar/{}?s=40&d=mm".format(hashlib.md5(self.email.encode('utf-8')).hexdigest())

    def save(self, *args, **kwargs):

        is_new = self.pk is None

        if not self.name:
            self.first_name = self.last_name = ''
        elif ' ' in self.name:
            self.first_name, self.last_name = self.name.split(' ', 1)
        else:
            self.first_name = self.name
            self.last_name = ''
        super().save(*args, **kwargs)

        if is_new and settings.REGISTRATION_DEFAULT_GROUP_NAME:
            try:
                self.groups.add(Group.objects.get(name=settings.REGISTRATION_DEFAULT_GROUP_NAME))
                self.save()
            except Group.DoesNotExist:
                pass

    def get_picture_url(self):
        if not self.image:
            location = "/%s/%s" % (settings.STATIC_URL, 'img/avatar-default.png')
        else:
            location = "/%s/%s" % (settings.MEDIA_URL, self.image)
        return re.sub('/+', '/', location)

    def get_picture_thumb_url(self,
                              options={'size': (150, 150), 'crop': 'scale'}):
        try:
            return get_thumbnailer(self.image).get_thumbnail(options).url
        except InvalidImageFormatError:
            return str(settings.STATIC_URL + 'img/avatar-default.png')

    def get_user_type(self):
        if self.is_superuser:
            return "superuser"
        elif self.groups.filter(name='professors').count():
            return "professors"
        elif self.groups.filter(name='students').count():
            return "students"
        return "unidentified"

    @property
    def is_profile_filled(self):

        for field in settings.ACCOUNT_REQUIRED_FIELDS:
            try:
                f = getattr(self, field)
                if not f:
                    return False
            except AttributeError:
                raise AttributeError(_('Invalid attribute: %s' % field))
        return True

    def get_certificates(self):
        from courses.certification.models import CourseCertification
        return CourseCertification.objects.filter(course_student__user=self)


class UserLogin(models.Model):
    """Represent users' logins, one per record"""
    user = models.ForeignKey(
        User,
        models.CASCADE,
    )
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        name = self.user.name or self.user.username
        return name + ' - ' + str(self.timestamp)

