from urllib.parse import parse_qs

import requests
from django.urls import reverse
from django.http import Http404, JsonResponse
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.contrib.admin.models import LogEntry, CHANGE
from django.contrib.contenttypes.models import ContentType
from django.db.models import Q
from django.http import HttpResponse

from requests_oauthlib import OAuth1
from rest_framework.decorators import detail_route, parser_classes, action, list_route
from rest_framework.response import Response
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.authtoken.models import Token
from rest_framework import status, generics, reverse, viewsets, permissions, mixins, filters
from rest_framework.pagination import PageNumberPagination

from django_filters.rest_framework import DjangoFilterBackend

from allauth.socialaccount.models import SocialApp
from allauth.socialaccount.providers.facebook.views import FacebookOAuth2Adapter
from allauth.socialaccount.providers.twitter.views import TwitterOAuthAdapter
from rest_auth.views import LoginView
from rest_auth.registration.views import SocialLoginView, SocialConnectView
from rest_auth.social_serializers import TwitterLoginSerializer, TwitterConnectSerializer

from oauth2_provider.views import ProtectedResourceView
from oauth2_provider.models import AccessToken

from .permissions import IsCurrentUserOrAdmin
from .serializers import (
    UserSerializer,
    FixSocialLoginSerializer,
    SimpleUserSerializer,
    SearchUserSerializer,
    GroupAdminSerializer,
    GroupSerializer,
)

import json

User = get_user_model()


class UserViewSet(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()
    permission_classes = [
        permissions.IsAuthenticated,
        IsCurrentUserOrAdmin,
    ]

    def get_queryset(self):

        queryset = super().get_queryset()

        current_user = self.request.user
        if not current_user.is_authenticated:
            return queryset.filter(id=current_user.id)
        classes = current_user.professor_classes.all()

        # Ensure only Professors or admins may search for users
        if not (classes or current_user.is_staff or current_user.is_superuser):
            return queryset.filter(id=current_user.id)

        return queryset

    @action(detail=False)
    def current(self, request):
        if self.request.user.id is None:
            raise Http404
        return Response(UserSerializer(self.request.user).data)


class UserProfileViewSet(UserViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()
    permission_classes = [
        permissions.IsAuthenticated,
        IsCurrentUserOrAdmin,
    ]

    @detail_route(methods=['POST'])
    @parser_classes((FormParser, MultiPartParser))
    def image(self, request, *args, **kwargs):
        if 'image' in request.data:
            user_profile = self.get_object()
            user_profile.image.delete()

            upload = request.data['image']

            user_profile.image.save(upload.name, upload)

            return Response(status=status.HTTP_201_CREATED,
                            headers={'Location': user_profile.image.url})
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['POST'], permission_classes=[IsCurrentUserOrAdmin])
    def user_status(self, request):
        try:
            email = request.data.get('email', None)
            if email:
                user = User.objects.get(email=email)
                return Response({'user': email,
                                 'active': user.has_usable_password() or
                                           user.socialaccount_set.count() > 0,
                                 'exists':  True,},
                                status=status.HTTP_200_OK)
            else:
                return Response({'error': 'e-mail field required'},
                                status=status.HTTP_400_BAD_REQUEST)
        except User.DoesNotExist:
            return Response({
                'user': request.data['email'],
                'active': False,
                'exists': False,
            }, status=status.HTTP_200_OK)

    @list_route(methods=['POST'], permission_classes=[permissions.AllowAny])
    def register_user(self, request):
        captcha_valid = self.validate_captcha(request.data.get('token', ''))
        if not captcha_valid:
            return Response({
                'error': True,
                'message': 'invalid captcha'
            }, status=status.HTTP_200_OK)

        user_data = request.data.get('user', None)
        if not user_data:
            return Response({
                'error': True,
                'message': 'incomplete data set'
            }, status=status.HTTP_400_BAD_REQUEST)

        if User.objects.filter(email=user_data['email']).exists():
            user = User.objects.get(email=user_data['email'])
            if not user.zipcode:
                user.zipcode = user_data['zipcode']
            if not user.origin:
                user.origin = user_data['origin']
            if not user.city and user_data.get('city', None):
                user.city = City.objects.get(pk=user_data['city'])
            if not user.state and user_data.get('state', None):
                user.state = Region.objects.get(pk=user_data['state'])
            user.save()

            return Response({
                'error': True,
                'message': 'user already subscribed'
            }, status=status.HTTP_200_OK)

        user_data['username'] = get_valid_username(user_data['email'])

        if user_data.get('origin', None) and \
                user_data['origin'] in ['ocupa', 'manu']:
            serializer = SimpleUserRegistrationSerializer(data=user_data,
                                                    context={
                                                        'request': request
                                                    })
            if serializer.is_valid():
                serializer.save()
                return Response({
                    'error': False,
                    'data': serializer.data
                    }, status=status.HTTP_201_CREATED)
            else:
                return Response(serializer.errors,
                                status.HTTP_400_BAD_REQUEST)
        else:
            return Response({
                'error': True,
                'message': 'no origin sent'
            }, status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['GET'], permission_classes=[IsCurrentUserOrAdmin])
    def me(self, request):
        if self.request.user.id is None:
            raise Http404
        return Response(
            UserSerializer(self.request.user).data
        )

    def update(self, request, *args, **kwargs):
        message = self.get_change_message()
        partial = kwargs.pop('partial', False)
        instance = self.get_object()

        # If birth_date is empty, change the value to None to avoid a validation error
        try:
            if request.data['birth_date'] == '':
                request.data['birth_date'] = None
        except KeyError:
            pass

        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.request.user = serializer.save()

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        data = serializer.data

        LogEntry.objects.log_action(
            user_id         = request.user.pk,
            content_type_id = ContentType.objects.get_for_model(self.get_object()).pk,
            object_id       = self.get_object().pk,
            object_repr     = str(self.get_object()),
            action_flag     = CHANGE,
            change_message  = message
        )

        return Response(data)

    def create(self, request, *args, **kwargs):
        captcha_valid = self.validate_captcha(request.data.get('token', ''))
        if not captcha_valid:
            return Response({
                'captcha': ['invalid captcha']
            }, status=status.HTTP_400_BAD_REQUEST)

        return super(UserViewSet, self).create(request, *args, **kwargs)

    def get_changed(self):
        sent_data = self.request.data
        Serializer = self.get_serializer_class()
        obj = Serializer(self.get_object())
        instance = obj.data
        changed = []
        for key in sent_data:
            if key in instance and not(instance[key] == sent_data[key]):
                changed.append(key)

        return changed

    def get_change_message(self):
        changed = self.get_changed()
        if len(changed):
            message = 'Alterado {0}'.format(', '.join(changed))
        else:
            message = u''
        return message

    def get_permissions(self):
        if self.action == 'list':
            self.permission_classes = [permissions.IsAdminUser, ]
        elif self.action == 'retrieve':
            self.permission_classes = [IsCurrentUserOrAdmin]
        elif self.action == 'update':
            self.permission_classes = [IsCurrentUserOrAdmin]
        return super(UserViewSet, self).get_permissions()

    def validate_captcha(self, captcha_response):
        captcha_req = requests\
            .post('https://www.google.com/recaptcha/api/siteverify',
                  data = {
                      'secret': settings.RECAPTCHA_SECRET_KEY,
                      'response': captcha_response
                  })
        return captcha_req.json()['success']


class SearchUserViewSet(viewsets.ReadOnlyModelViewSet):
    model = User
    queryset = User.objects.all()
    serializer_class = SearchUserSerializer
    permission_classes = [
        IsCurrentUserOrAdmin,
        permissions.IsAuthenticated,
    ]
    lookup_field = 'id'
    search_fields = ('first_name', 'last_name', 'username', 'email', 'name',)
    filter_fields = ('groups__name',)
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter,)
    ordering = ( 'name', 'first_name', 'username',)

    def get_queryset(self):

        queryset = super().get_queryset()

        current_user = self.request.user
        classes = current_user.professor_classes.all()

        # Ensure only Professors or admins may search for users
        if not (classes or current_user.is_staff or is_superuser):
            return queryset.none()

        course = self.request.query_params.get('course', None)

        if classes:
            queryset = queryset.filter(classes__in=classes)
        else:
            # FIXME: if every student is in a class, this is useless.
            if course is not None:
                queryset = queryset.filter(studentcourse_set=course)
        
        query = self.request.query_params.get('name', None)

        if query is not None:
            queryset = queryset.filter(
                Q(name__icontains=query) |
                Q(first_name__icontains=query) |
                Q(last_name__icontains=query) |
                Q(username__icontains=query) |
                Q(email__icontains=query),
            )
        return queryset


class SimpleUserViewSet(generics.GenericAPIView, mixins.RetrieveModelMixin):
    serializer_class = SimpleUserSerializer
    queryset = User.objects.all()
    lookup_field = ('username', 'email')


class GroupPagination(PageNumberPagination):
    page_size = 50
    page_size_query_param = 'page_size'
    max_page_size = 100

class GroupAdminViewSet(viewsets.ModelViewSet):
    queryset = Group.objects.all().order_by('id')
    model = Group
    serializer_class = GroupAdminSerializer
    permission_classes = (permissions.IsAdminUser, )
    pagination_class = GroupPagination

    def put(self, request, **kwargs):
        User = get_user_model()
        # Does a user need to be removed from a given group?
        if request.data['action'] == 'remove':
            group = Group.objects.get(id=request.data['id'])
            group.user_set.remove(User.objects.get(id=request.data['user']['id']))
            return Response(status=200)

        # Does a user nedd to be added to a given group?
        # The "add" action support multiple users
        if request.data['action'] == 'add':
            group = Group.objects.get(id=request.data['id'])
            for user in request.data.get('users', None):
                group.user_set.add(User.objects.get(id=user['id']))
            return Response(status=200)

        if request.data['action'] == 'bulk_remove':
            group = Group.objects.get(id=request.data['id'])
            users = User.objects.filter(email__in=request.data['users'])
            for user in users:
                group.user_set.remove(user)
            return Response(status=200)

        if request.data['action'] == 'bulk_add':
            group = Group.objects.get(id=request.data['id'])
            users = User.objects.filter(email__in=request.data['users'])
            for user in users:
                group.user_set.add(user)
            return Response(status=200)

        return Response(status=404)


class GroupViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows groups to be viewed.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


# FIXME: the social classes may need rework when updating to v0.9.3
#        FixSocialLoginSerializer class may not be needed anymore
class FacebookLogin(SocialLoginView):
    adapter_class = FacebookOAuth2Adapter
    serializer_class = FixSocialLoginSerializer


class TwitterLogin(LoginView):
    serializer_class = TwitterLoginSerializer
    adapter_class = TwitterOAuthAdapter


class FacebookConnect(SocialConnectView):
    adapter_class = FacebookOAuth2Adapter


class TwitterConnect(SocialConnectView):
    serializer_class = TwitterConnectSerializer
    adapter_class = TwitterOAuthAdapter


class TwitterRequestToken(generics.GenericAPIView):
    def post(self, request):
        if not SocialApp.objects.filter(provider='twitter').exists():
            return Response({'message': _('No Twitter provider found')},
                            status=status.HTTP_404_NOT_FOUND)
        twitter_app = SocialApp.objects.get(provider='twitter')
        CONSUMER_KEY = twitter_app.client_id
        CONSUMER_SECRET = twitter_app.secret

        callback = request\
            .build_absolute_uri(reverse('twitter_connect_callback'))
        auth = OAuth1(CONSUMER_KEY, CONSUMER_SECRET, callback_uri=callback)
        r = requests.post('https://api.twitter.com/oauth/request_token',
                          auth=auth)
        if r.status_code == 200:
            data = parse_qs(r.content.decode('utf-8'))
        else:
            return Response({'message': r.content},
                            status.HTTP_400_BAD_REQUEST)

        return Response(data, status=status.HTTP_200_OK)


class TwitterAccessToken(generics.GenericAPIView):
    def post(self, request):
        if not SocialApp.objects.filter(provider='twitter').exists():
            return Response({'error': ('No Twitter provider found')},
                            status=status.HTTP_404_NOT_FOUND)
        twitter_app = SocialApp.objects.get(provider='twitter')
        CONSUMER_KEY = twitter_app.client_id
        CONSUMER_SECRET = twitter_app.secret

        auth = OAuth1(CONSUMER_KEY, CONSUMER_SECRET,
                      request.data['oauth_token'],
                      verifier=request.data['oauth_verifier'])
        r = requests\
            .post('https://api.twitter.com/oauth/access_token', auth=auth)
        if r.status_code == 200:
            data = parse_qs(r.content.decode('ascii'))
        else:
            return Response({'message': r.content},
                            status.HTTP_400_BAD_REQUEST)

        return Response(data, status=status.HTTP_200_OK)


def get_api_key(request):
    if request.user.id is None:
        raise Http404

    token = Token.objects.get_or_create(user=request.user)
    return JsonResponse({'key': token[0].key}, status=200)


class OAuth2UserInfoView(ProtectedResourceView):
    def get(self, request, *args, **kwargs):
        token = request.GET.get('access_token', '')
        access_token = AccessToken.objects.filter(token=token).first()

        if access_token:
            user = access_token.user
            return HttpResponse(json.dumps({
                'id': user.id,
                'username': user.username,
                'email': user.email,
                'name': user.name,
            }))

        return HttpResponseForbidden('Ivalid or empty token')
