from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import get_user_model
from django.conf import settings
from rest_framework import serializers
from rest_auth.registration.serializers import SocialLoginSerializer, RegisterSerializer
from allauth.account.adapter import get_adapter
from allauth.account.utils import setup_user_email
from allauth.account import app_settings as allauth_settings
from allauth.socialaccount.helpers import complete_social_login
from requests.exceptions import HTTPError
from cities_light.models import City, Region
from courses.models import CourseStudent
from courses.certification.models import CourseCertification
from django.contrib.auth.models import Group

import requests


User = get_user_model()


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = (
            'id', 'username', 'name', 'email', 'image',
            'biography', 'city', 'state', 'country', 'race', 'gender',
            'occupation', 'is_superuser', 'is_staff', 'is_active',
            'is_profile_filled', 'groups', 'cpf',  'phone_number',
            'sexual_orientation', 'sexual_orientation_other', 'birth_date',
            'address', 'address_extra', 'address_number', 'zipcode',
        )


class SimpleUserSerializer(serializers.ModelSerializer):

    is_profile_filled = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ('id', 'image', 'name', 'email', 'biography',
                  'is_profile_filled', 'username')

    def get_is_profile_filled(self, obj):
        return obj.is_profile_filled


class ClassSimpleUserSerializer(SimpleUserSerializer):

    certificate = serializers.SerializerMethodField()
    can_emmit_receipt = serializers.SerializerMethodField()

    class Meta(SimpleUserSerializer.Meta):
        fields = SimpleUserSerializer.Meta.fields + ('certificate', 'can_emmit_receipt',)

    def get_certificate(self, obj):
        course_id = self.context.get('course_id', '')
        if course_id:
            course_student = CourseStudent.objects.filter(user=obj.id, course=course_id)

            if course_student and CourseCertification.objects.filter(course_student=course_student.first().id).exists():
                course_cert = CourseCertification.objects.filter(course_student=course_student.first().id).first()
                link_hash = course_cert.link_hash
                cert_type = course_cert.type
                return { 'exists': True, 'link_hash': link_hash, 'type': cert_type }

        return { 'exists': False, 'link_hash': '', 'type': ''}

    def get_can_emmit_receipt(self, obj):
        course_id = self.context.get('course_id', '')
        if course_id:
            course_student = CourseStudent.objects.filter(user=obj.id, course=course_id)

            if course_student:
                return course_student.first().can_emmit_receipt()

        return False


class SearchUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = (
            'id', 'email', 'name', 'first_name', 'last_name',
            'image', 'groups', 'username',
            'biography',
        )


# class TimtecUserSerializer(serializers.ModelSerializer):
#     name = serializers.ReadOnlyField(source='get_full_name')
#     picture = serializers.ReadOnlyField(source='get_picture_thumb_url')
#     is_profile_filled = serializers.BooleanField()

#     class Meta:
#         model = get_user_model()
#         fields = ('id', 'username', 'email', 'name', 'first_name', 'last_name',
#                   'biography', 'picture', 'is_profile_filled')


class UserAdminSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'username', 'name', 'email', 'is_active', 'is_superuser', 'first_name', 'last_name', 'image', 'groups', )
        depth = 1


class TimtecUserAdminCertificateSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'name', 'email', 'username')


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ('id', 'name')


class GroupAdminSerializer(GroupSerializer):

    users = SimpleUserSerializer(many=True, read_only=True, source="user_set")

    class Meta(GroupSerializer.Meta):
        fields = ('id', 'name', 'users', )
        depth = 1


class RegistrationSerializer(RegisterSerializer):

    name = serializers.CharField(max_length=255, required=False)

    captcha = serializers.CharField(max_length=2048)
    # city = serializers.PrimaryKeyRelatedField(queryset=City.objects.all(),
    #                                           allow_null=True)
    # state = serializers.PrimaryKeyRelatedField(queryset=Region.objects.all(),
    #                                            allow_null=True)
    # phone_number = serializers.CharField(max_length=255, allow_null=True)
    origin = serializers.CharField(max_length=30, required=False, allow_blank=True)
    accepted_terms = serializers.BooleanField()

    def get_cleaned_data(self):
        return {
            'name': self.validated_data.get('name', ''),
            'username': self.validated_data.get('username', ''),
            'password1': self.validated_data.get('password1', ''),
            'email': self.validated_data.get('email', ''),
            # 'city': self.validated_data.get('city', None),
            # 'state': self.validated_data.get('state', None),
            # 'phone_number': self.validated_data.get('phone_number', ''),
            'origin': self.validated_data.get('origin', 'signup'),
            'accepted_terms': self.validated_data.get('accepted_terms', ''),
        }

    def validate_captcha(self, attr):
        captcha_req = requests.post('https://www.google.com/recaptcha/api/siteverify',
            data = {
                'secret': settings.RECAPTCHA_SECRET_KEY,
                'response': attr
            })
        captcha_resp = captcha_req.json()
        print(settings.RECAPTCHA_SECRET_KEY)
        print(captcha_resp)
        if not captcha_resp['success']:
            raise serializers.ValidationError(_('Invalid CAPTCHA.'))

    def custom_signup(self, request, user):
        user.name = self.cleaned_data['name']
        # user.phone_number = self.cleaned_data['phone_number']
        # user.city = self.cleaned_data['city']
        # user.state = self.cleaned_data['state']
        user.accepted_terms = self.cleaned_data['accepted_terms']
        user.origin = self.cleaned_data['origin']
        user.save()

    def save(self, request):
        adapter = get_adapter()
        user = adapter.new_user(request)
        self.cleaned_data = self.get_cleaned_data()
        adapter.save_user(request, user, self)
        self.custom_signup(request, user)
        setup_user_email(request, user, [])
        return user


class FixSocialLoginSerializer(SocialLoginSerializer):
        def validate(self, attrs):
            view = self.context.get('view')
            request = self._get_request()

            if not view:
                raise serializers.ValidationError(
                    _("View is not defined, pass it as a context variable")
                )

            adapter_class = getattr(view, 'adapter_class', None)
            if not adapter_class:
                raise serializers.ValidationError(_("Define adapter_class in view"))

            adapter = adapter_class(request)
            app = adapter.get_provider().get_app(request)

            # More info on code vs access_token
            # http://stackoverflow.com/questions/8666316/facebook-oauth-2-0-code-and-token

            # Case 1: We received the access_token
            if attrs.get('access_token'):
                access_token = attrs.get('access_token')

            # Case 2: We received the authorization code
            elif attrs.get('code'):
                self.callback_url = getattr(view, 'callback_url', None)
                self.client_class = getattr(view, 'client_class', None)

                if not self.callback_url:
                    raise serializers.ValidationError(
                        _("Define callback_url in view")
                    )
                if not self.client_class:
                    raise serializers.ValidationError(
                        _("Define client_class in view")
                    )

                code = attrs.get('code')

                provider = adapter.get_provider()
                scope = provider.get_scope(request)
                client = self.client_class(
                    request,
                    app.client_id,
                    app.secret,
                    adapter.access_token_method,
                    adapter.access_token_url,
                    self.callback_url,
                    scope
                )
                token = client.get_access_token(code)
                access_token = token['access_token']

            else:
                raise serializers.ValidationError(
                    _("Incorrect input. access_token or code is required."))

            social_token = adapter.parse_token({'access_token': access_token})
            social_token.app = app

            try:
                login = self.get_social_login(adapter, app, social_token, access_token)
                complete_social_login(request, login)
            except HTTPError:
                raise serializers.ValidationError(_('Incorrect value'))

            if not login.is_existing:
                # We have an account already signed up in a different flow
                # with the same email address: raise an exception.
                # This needs to be handled in the frontend. We can not just
                # link up the accounts due to security constraints
                if(allauth_settings.UNIQUE_EMAIL):
                    if login.user.email and get_user_model().objects.filter(
                        email=login.user.email,
                    ).exists():
                        # There is an account already
                        raise serializers.ValidationError(
                                          ("A user is already registered with this e-mail address."))

                login.lookup()
                login.save(request, connect=True)
            attrs['user'] = login.account.user

            return attrs
