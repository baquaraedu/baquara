from django.views.generic.base import TemplateView
from rest_auth.registration.views import SocialAccountListView, \
    SocialAccountDisconnectView
from rest_framework.routers import SimpleRouter
from django.conf.urls import url

from . import views

router = SimpleRouter()
router.register(r'users', views.UserViewSet)
# This "profile" view is only for legacy porpouses, don´t user it anymore!!! 
router.register(r'profile', views.UserProfileViewSet)
router.register(r'users/search', views.SearchUserViewSet)
# router.register(r'stats', views.StatsViewSet, base_name='user_stats'),

app_name = 'users'
urlpatterns = [
    url(
        regex=r'^key/$',
        view=views.get_api_key,
        name='api-key'
    ),
    url(
        regex=r'^details/(?P<username>.*)/$',
        view=views.SimpleUserViewSet,
        name='user-detail'
    ),
    url(
        r'^close/$',
        TemplateView.as_view(template_name='users/close.html')
    ),
    url(
        r'^socialaccounts/$',
        SocialAccountListView.as_view(),
        name='social_account_list'
    ),
    url(
        r'^socialaccounts/(?P<pk>\d+)/disconnect/$',
        SocialAccountDisconnectView.as_view(),
        name='social_account_disconnect'
    )
]

urlpatterns.extend(router.urls)
