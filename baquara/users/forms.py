from allauth.account.forms import SignupForm
from django import forms
from baquara.users.models import User, GenderOption, SexualOrientationOption, RaceOption
from django.utils.translation import gettext_lazy as _
from cities_light.models import City

class DateInput(forms.DateInput):
    input_type = 'date'

class CustomSignupForm(SignupForm):
    preferred_language = forms.ChoiceField(choices=User.LANGUAGES_CHOICES)
    accepted_terms = forms.BooleanField(initial=False, required=False)
    birth_date = forms.DateField( required=True, widget=DateInput(
        attrs={
            'title' : _('Birth Date'),
            'class':'form-control',
        }
    ))
    name = forms.CharField(max_length=128)
    gender = forms.ModelChoiceField(queryset = GenderOption.objects.all(),empty_label=_("Select your gender"), widget=forms.Select(
        attrs={
            'class':'form-control',
        }
    ) )
    city = forms.ModelChoiceField(queryset = City.objects.all(),empty_label=_("Inform your town of residence"), widget=forms.Select(
        attrs={
            'class':'form-control',
        }
    ) )
    race = forms.ModelChoiceField(queryset = RaceOption.objects.all(), empty_label=_("Tell us how you identify yourself"), widget=forms.Select(
        attrs={
            'class':'form-control',
        }
    ) )

    living_area_choices = ( ('', _('Inform if you live in an urban or rural location') ), ) + User.LIVING_AREA_CHOICES
    living_area = forms.ChoiceField(
        required=True,
        choices=living_area_choices,
        widget=forms.Select(
            attrs={
                'class':'form-control',
                'placeholder' : _('Inform if you live in an urban or rural location')
            }
        )
    )

    def save(self, request):
        user = super(CustomSignupForm, self).save(request)
        user.gender = self.cleaned_data['gender']
        user.race = self.cleaned_data['race']
        user.birth_date = self.cleaned_data['birth_date']
        user.living_area = self.cleaned_data['living_area']
        user.city = self.cleaned_data['city']
        user.preferred_language = self.cleaned_data['preferred_language']
        user.accepted_terms = self.cleaned_data['accepted_terms']
        user.name = self.cleaned_data['name']
        user.save()

        return user
