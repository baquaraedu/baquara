# Generated by Django 2.2.28 on 2022-05-23 20:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0012_auto_20220523_1429'),
    ]

    operations = [
        migrations.AddField(
            model_name='genderoption',
            name='name_en',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Option Name'),
        ),
        migrations.AddField(
            model_name='genderoption',
            name='name_es',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Option Name'),
        ),
        migrations.AddField(
            model_name='genderoption',
            name='name_pt_br',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Option Name'),
        ),
    ]
