# Generated by Django 2.2.25 on 2022-05-18 00:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0007_auto_20220517_1230'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='living_area',
            field=models.CharField(blank=True, choices=[('urban', 'Urban'), ('rural', 'Rural')], max_length=255, null=True, verbose_name='Living Area'),
        ),
    ]
