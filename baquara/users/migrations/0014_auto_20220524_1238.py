# Generated by Django 2.2.28 on 2022-05-24 15:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0013_auto_20220523_1738'),
    ]

    operations = [
        migrations.AddField(
            model_name='raceoption',
            name='name_en',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Option Name'),
        ),
        migrations.AddField(
            model_name='raceoption',
            name='name_es',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Option Name'),
        ),
        migrations.AddField(
            model_name='raceoption',
            name='name_pt_br',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Option Name'),
        ),
        migrations.AddField(
            model_name='sexualorientationoption',
            name='name_en',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Option Name'),
        ),
        migrations.AddField(
            model_name='sexualorientationoption',
            name='name_es',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Option Name'),
        ),
        migrations.AddField(
            model_name='sexualorientationoption',
            name='name_pt_br',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Option Name'),
        ),
    ]
