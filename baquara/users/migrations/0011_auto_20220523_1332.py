# Generated by Django 2.2.28 on 2022-05-23 16:32

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0010_user_living_area'),
    ]

    operations = [
        migrations.CreateModel(
            name='GenderOption',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=255, null=True, verbose_name='Option Name')),
            ],
        ),
        migrations.CreateModel(
            name='RaceOption',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=255, null=True, verbose_name='Option Name')),
            ],
        ),
        migrations.AlterField(
            model_name='user',
            name='gender',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='users.GenderOption', verbose_name='Gender'),
        ),
        migrations.AlterField(
            model_name='user',
            name='race',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='users.RaceOption', verbose_name='Race'),
        ),
    ]
