# Generated by Django 2.2.22 on 2021-05-11 20:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0004_user_social_instagram'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='preferred_language',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Preferred language'),
        ),
    ]
