from modeltranslation.translator import register, TranslationOptions
from .models import GenderOption, SexualOrientationOption, RaceOption

@register(GenderOption)
class GenderOptionTranslationOptions(TranslationOptions):
    fields = ('name',)

@register(SexualOrientationOption)
class SexualOrientationOptionTranslationOptions(TranslationOptions):
    fields = ('name',)
    
@register(RaceOption)
class RaceOptionTranslationOptions(TranslationOptions):
    fields = ('name',)