# Pro-tip: Try not to put anything here. Avoid dependencies in
#	production that aren't in development.
-r base.txt

# WSGI Handler
# ------------------------------------------------
# gevent==1.4.0
gunicorn==20.0.4

# Static and Media Storage
# ------------------------------------------------
# boto3==1.5.23
# django-storages==1.6.5
# Collectfast==0.6.0

# Email backends for Mailgun, Postmark, SendGrid and more
# -------------------------------------------------------
django-anymail==5.0

# Raven is the Sentry client
# --------------------------
sentry-sdk==0.17.6

# <external_app>
git+https://git.hacklab.com.br/hackmooc/django-courses.git@v1.10.1#egg=django-courses
git+https://git.hacklab.com.br/hackmooc/django-courses-learning-objects.git@v0.1.4#egg=django-courses-learning-objects
git+https://git.hacklab.com.br/hackmooc/django-courses-legacy.git@v1.7.0#egg=django-courses-legacy
git+https://git.hacklab.com.br/hackmooc/django-courses-notifications.git@v0.1.2#egg=django-courses-notifications

git+https://github.com/hacklabr/django-discussion.git@v0.13.4#egg=django-discussion
git+https://github.com/hacklabr/django-cards.git@v1.0.0#egg=django-cards

git+https://gitlab.com/baquaraedu/django-courses-frontend.git@v1.14.14#egg=django-courses-frontend
